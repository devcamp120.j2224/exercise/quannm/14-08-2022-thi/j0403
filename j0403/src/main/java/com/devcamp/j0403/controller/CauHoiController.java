package com.devcamp.j0403.controller;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j0403.model.CauHoi;
import com.devcamp.j0403.repository.CauHoiRepository;

@CrossOrigin
@RestController
public class CauHoiController {
    @Autowired
    CauHoiRepository pCauHoiRepository;

    @GetMapping("/question/all")
    public List<CauHoi> getAllQuestion() {
        return pCauHoiRepository.findAll();
    }

    @GetMapping("/question/details/{id}")
    public CauHoi getQuestionById(@PathVariable Long id) {
        if (pCauHoiRepository.findById(id).isPresent()) {
            return pCauHoiRepository.findById(id).get();
        } else {
            return null;
        }
    }

	@GetMapping("/question/answer/{id}")
	public CauHoi getQuestionByAnswerId(@PathVariable Long id) {
		return pCauHoiRepository.findByDapAnId(id);
	}

    @PostMapping("/question/create")
    public ResponseEntity<Object> createQuestion(@Valid @RequestBody CauHoi questionObj) {
        try {
            CauHoi newQues = new CauHoi();
            newQues.setDiem(questionObj.getDiem());
            newQues.setMaCauHoi(questionObj.getMaCauHoi());
            newQues.setMonHoc(questionObj.getMonHoc());
            newQues.setNoiDungCauHoi(questionObj.getNoiDungCauHoi());
            newQues.setDapAn(questionObj.getDapAn());
            newQues.setNgayTao(new Date());
            CauHoi saveQues = pCauHoiRepository.save(newQues);
            return new ResponseEntity<>(saveQues, HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Question: "+e.getCause().getCause().getMessage());
        }
    }

    @CrossOrigin
	@PutMapping("/question/update/{id}")
	public ResponseEntity<Object> updateQuestion(@Valid @PathVariable("id") Long id, @RequestBody CauHoi questionObj) {
		Optional<CauHoi> questionData = pCauHoiRepository.findById(id);
		if (questionData.isPresent()) {
			try {
                CauHoi newQuestion = questionData.get();
                newQuestion.setDiem(questionObj.getDiem());
                newQuestion.setMaCauHoi(questionObj.getMaCauHoi());
                newQuestion.setMonHoc(questionObj.getMonHoc());
                newQuestion.setNoiDungCauHoi(questionObj.getNoiDungCauHoi());
                newQuestion.setDapAn(questionObj.getDapAn());
                CauHoi saveQuestion = pCauHoiRepository.save(newQuestion);
                return new ResponseEntity<>(saveQuestion, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @CrossOrigin
	@DeleteMapping("/question/delete/{id}")
	public ResponseEntity<Object> deleteQuestionById(@PathVariable Long id) {
		try {
			pCauHoiRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
