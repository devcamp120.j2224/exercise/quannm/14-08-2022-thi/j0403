package com.devcamp.j0403.controller;

import com.devcamp.j0403.model.DapAn;
import com.devcamp.j0403.repository.DapAnRepository;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class DapAnController {
    @Autowired
    DapAnRepository pDapAnRepository;

    @GetMapping("/answer/all")
    public List<DapAn> getAllAnswer() {
        return pDapAnRepository.findAll();
    }

    @GetMapping("/answer/details/{id}")
    public DapAn getAnswerById(@PathVariable Long id) {
        if (pDapAnRepository.findById(id).isPresent()) {
            return pDapAnRepository.findById(id).get();
        } else {
            return null;
        }
    }

    @PostMapping("/answer/create")
    public ResponseEntity<Object> createAnswer(@Valid @RequestBody DapAn answerObj) {
        try {
            DapAn newAns = new DapAn();
            newAns.setMaDapAn(answerObj.getMaDapAn());
            newAns.setNoiDungDapAn(answerObj.getNoiDungDapAn());
            newAns.setKetQuaDapAn(answerObj.isKetQuaDapAn());
            newAns.setGiaiThich(answerObj.getGiaiThich());
            newAns.setCauHoi(answerObj.getCauHoi());
            DapAn saveAns = pDapAnRepository.save(newAns);
            return new ResponseEntity<>(saveAns, HttpStatus.CREATED);
        } catch (Exception e) {
            //TODO: handle exception
            System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Answer: "+e.getCause().getCause().getMessage());
        }
    }

    @CrossOrigin
	@PutMapping("/answer/update/{id}")
	public ResponseEntity<Object> updateAnswer(@Valid @PathVariable("id") Long id, @RequestBody DapAn answerObj) {
		Optional<DapAn> answerData = pDapAnRepository.findById(id);
		if (answerData.isPresent()) {
			try {
                DapAn newAns = answerData.get();
                newAns.setMaDapAn(answerObj.getMaDapAn());
                newAns.setNoiDungDapAn(answerObj.getNoiDungDapAn());
                newAns.setKetQuaDapAn(answerObj.isKetQuaDapAn());
                newAns.setGiaiThich(answerObj.getGiaiThich());
                newAns.setCauHoi(answerObj.getCauHoi());
                DapAn saveAns = pDapAnRepository.save(newAns);
                return new ResponseEntity<>(saveAns, HttpStatus.OK);
            } catch (Exception e) {
                //TODO: handle exception
                return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
            }
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @CrossOrigin
	@DeleteMapping("/answer/delete/{id}")
	public ResponseEntity<Object> deleteAnswerById(@PathVariable Long id) {
		try {
			pDapAnRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
