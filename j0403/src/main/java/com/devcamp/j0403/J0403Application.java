package com.devcamp.j0403;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J0403Application {

	public static void main(String[] args) {
		SpringApplication.run(J0403Application.class, args);
	}

}
