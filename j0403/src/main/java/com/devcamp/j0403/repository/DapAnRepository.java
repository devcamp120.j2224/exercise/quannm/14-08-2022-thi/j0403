package com.devcamp.j0403.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.j0403.model.DapAn;

@Repository
public interface DapAnRepository extends JpaRepository<DapAn, Long> {
    
}
