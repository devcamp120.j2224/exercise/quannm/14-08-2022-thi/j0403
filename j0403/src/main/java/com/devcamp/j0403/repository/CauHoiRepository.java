package com.devcamp.j0403.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.j0403.model.CauHoi;

@Repository
public interface CauHoiRepository extends JpaRepository<CauHoi, Long> {
    CauHoi findByDapAnId(Long id);
}
