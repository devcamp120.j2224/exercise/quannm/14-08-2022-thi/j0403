package com.devcamp.j0403.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "dap_an")
public class DapAn {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Mã đáp án không được để trống!")
    @Column(unique = true, name = "ma_dap_an")
    private String maDapAn;

    @NotEmpty(message = "Nội dung đáp án không được để trống!")
    @Column(name = "noi_dung_dap_an")
    private String noiDungDapAn;

    @Column(name = "ket_qua_dap_an")
    private Boolean ketQuaDapAn;

    @Column(name = "giai_thich")
    private String giaiThich;
    
    @ManyToOne
    private CauHoi cauHoi;

    public DapAn() {
    }

    public DapAn(Long id, String maDapAn, String noiDungDapAn, Boolean ketQuaDapAn, String giaiThich, CauHoi cauHoi) {
        this.id = id;
        this.maDapAn = maDapAn;
        this.noiDungDapAn = noiDungDapAn;
        this.ketQuaDapAn = ketQuaDapAn;
        this.giaiThich = giaiThich;
        this.cauHoi = cauHoi;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMaDapAn() {
        return maDapAn;
    }

    public void setMaDapAn(String maDapAn) {
        this.maDapAn = maDapAn;
    }

    public String getNoiDungDapAn() {
        return noiDungDapAn;
    }

    public void setNoiDungDapAn(String noiDungDapAn) {
        this.noiDungDapAn = noiDungDapAn;
    }

    public Boolean isKetQuaDapAn() {
        return ketQuaDapAn;
    }

    public void setKetQuaDapAn(Boolean ketQuaDapAn) {
        this.ketQuaDapAn = ketQuaDapAn;
    }

    public String getGiaiThich() {
        return giaiThich;
    }

    public void setGiaiThich(String giaiThich) {
        this.giaiThich = giaiThich;
    }
    
    public CauHoi getCauHoi() {
        return cauHoi;
    }

    public void setCauHoi(CauHoi cauHoi) {
        this.cauHoi = cauHoi;
    }
    
}
