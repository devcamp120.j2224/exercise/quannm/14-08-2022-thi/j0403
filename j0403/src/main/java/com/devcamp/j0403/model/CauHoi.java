package com.devcamp.j0403.model;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import org.springframework.data.annotation.CreatedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "cau_hoi")
public class CauHoi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty(message = "Mã câu hỏi không được để trống!")
    @Column(name = "ma_cau_hoi", unique = true)
    private String maCauHoi;

    @NotEmpty(message = "Nội dung câu hỏi không được để trống!")
    @Column(name = "noi_dung_cau_hoi")
    private String noiDungCauHoi;

    @Column(name = "diem")
    private float diem;

    @Column(name = "mon_hoc")
    private String monHoc;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ngay_tao",updatable = false,nullable = true)
    @CreatedDate
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date ngayTao;

    @OneToMany(targetEntity = DapAn.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "cau_hoi_id")
    @JsonIgnore
    private Set<DapAn> dapAn;

    public CauHoi() {
    }

    public CauHoi(Long id, String maCauHoi, String noiDungCauHoi, float diem, String monHoc, Date ngayTao,
            Set<DapAn> dapAn) {
        this.id = id;
        this.maCauHoi = maCauHoi;
        this.noiDungCauHoi = noiDungCauHoi;
        this.diem = diem;
        this.monHoc = monHoc;
        this.ngayTao = ngayTao;
        this.dapAn = dapAn;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMaCauHoi() {
        return maCauHoi;
    }

    public void setMaCauHoi(String maCauHoi) {
        this.maCauHoi = maCauHoi;
    }

    public String getNoiDungCauHoi() {
        return noiDungCauHoi;
    }

    public void setNoiDungCauHoi(String noiDungCauHoi) {
        this.noiDungCauHoi = noiDungCauHoi;
    }

    public float getDiem() {
        return diem;
    }

    public void setDiem(float diem) {
        this.diem = diem;
    }

    public String getMonHoc() {
        return monHoc;
    }

    public void setMonHoc(String monHoc) {
        this.monHoc = monHoc;
    }

    public Date getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(Date ngayTao) {
        this.ngayTao = ngayTao;
    }

    @JsonIgnore
    public Set<DapAn> getDapAn() {
        return dapAn;
    }

    public void setDapAn(Set<DapAn> dapAn) {
        this.dapAn = dapAn;
    }
    
}
