$(document).ready(function() {
    // Vùng 1: Khai báo biến toàn cục
    var gArrayQuestion;
    var gIdDelete;
    var gNameCol = ["id", "maCauHoi", "monHoc", "diem", "noiDungCauHoi", "ngayTao", "action"];
    const gID_COL = 0;
    const gMA_CAU_HOI_COL = 1;
    const gMON_HOC_COL = 2;
    const gDIEM_cOL = 3;
    const gNOI_DUNG_CAU_HOI_COL = 4;
    const gNGAY_TAO_COL = 5;
    const gACTION_COL = 6;
    var gTableQuestion = $('#question-table').DataTable({
        data: gArrayQuestion,
        columns: [
            {data: gNameCol[gID_COL]},
            {data: gNameCol[gMA_CAU_HOI_COL]},
            {data: gNameCol[gMON_HOC_COL]},
            {data: gNameCol[gDIEM_cOL]},
            {data: gNameCol[gNOI_DUNG_CAU_HOI_COL]},
            {data: gNameCol[gNGAY_TAO_COL]},
            {data: gNameCol[gACTION_COL]}
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<button id='btn-update' class='btn btn-primary'>Update</button> &nbsp; <button id='btn-delete' class='btn btn-danger'>Delete</button>"
            }
        ]
    });

    // Vùng 2: Vùng gán
    onPageLoading();
    $('#btn-create-question').on('click', onBtnCreateQuestionClick);
    $('#question-table').on('click', '#btn-update', function() {
        onBtnUpdateQuestion(this);
    });
    $('#btn-update-question-modal').on('click', onBtnUpdateQuestionInModalClick);
    $('#question-table').on('click', '#btn-delete', function() {
        onBtnDeleteQuestion(this);
    });
    $('#btn-delete-question-modal').on('click', onBtnDeleteQuestionInModalClick);

    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
        callApiGetAllQuestion();
        loadDataToTable();
    }
    // Hàm xử lý khi click btn create
    function onBtnCreateQuestionClick() {
        var vQuesObj = {
            maCauHoi: "",
            noiDungCauHoi: "",
            diem: 0,
            monHoc: ""
        };
        vQuesObj.maCauHoi = $('#input-question-code').val();
        vQuesObj.noiDungCauHoi = $('#input-question-content').val();
        vQuesObj.diem = $('#input-point').val();
        vQuesObj.monHoc = $('#input-subject').val();
        console.log(vQuesObj);
        var vIsCheck = validateData(vQuesObj);
        if (vIsCheck == true) {
            callApiPostNewQuestion(vQuesObj);
        }
    }
    // Hàm xử lý khi click btn update
    function onBtnUpdateQuestion(paramEle) {
        var vTableRow = $(paramEle).parents("tr");
        var vQuestionRowData = gTableQuestion.row(vTableRow).data();
        console.log(vQuestionRowData);
        $('#modal-update-question').modal("show");
        showDataToModalUpdate(vQuestionRowData);
    }
    // Hàm xử lý khi click update in modal
    function onBtnUpdateQuestionInModalClick() {
        var vQuesObj = {
            id: 0,
            maCauHoi: "",
            noiDungCauHoi: "",
            diem: 0,
            monHoc: ""
        };
        vQuesObj.id = $('#inp-modal-question-id').val();
        vQuesObj.maCauHoi = $('#inp-modal-question-code').val();
        vQuesObj.noiDungCauHoi = $('#inp-modal-question-content').val();
        vQuesObj.diem = $('#inp-modal-point').val();
        vQuesObj.monHoc = $('#inp-modal-subject').val();
        console.log(vQuesObj);
            var vIsCheck = validateData(vQuesObj);
            if (vIsCheck == true) {
                callApiPutQuestion(vQuesObj);
                $('#modal-update-question').modal("hide");
            }
    }
    // Hàm xử lý khi click btn delete
    function onBtnDeleteQuestion(paramEle) {
        var vTableRow = $(paramEle).parents("tr");
        var vQuestionRowData = gTableQuestion.row(vTableRow).data();
        gIdDelete = vQuestionRowData.id;
        console.log(gIdDelete);
        $('#modal-delete-question').modal("show");
    }
    // Hàm xử lý khi confirm delete
    function onBtnDeleteQuestionInModalClick() {
        callApiDeleteQuestion();
        $('#modal-delete-question').modal("hide");
    }

    // Vùng 4: Khai báo hàm dùng chung
    // call api get all question
    function callApiGetAllQuestion() {
        $.ajax({
            url: "http://localhost:8080/question/all",
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res) {
                gArrayQuestion = res;
                console.log(gArrayQuestion);
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
    // Đổ dữ liệu vào datatable
    function loadDataToTable() {
        gTableQuestion.clear();
        gTableQuestion.rows.add(gArrayQuestion);
        gTableQuestion.draw();
    }
    // Validate data
    function validateData(paramData) {
        var vResult = true;
        if (paramData.maCauHoi == "") {
            alert("Question code chưa nhập!");
            vResult = false;
        }
        if (paramData.noiDungCauHoi == "") {
            alert("Question content chưa nhập!");
            vResult = false;
        }
        if (paramData.diem == "") {
            alert("Point chưa nhập!")
            vResult = false;
        }
        if (paramData.diem < 0 || paramData.diem > 10) {
            alert("Point phải lớn hơn 0 và nhỏ hơn 10!")
            vResult = false;
        }
        if (paramData.monHoc == "") {
            alert("Subject chưa nhập!")
            vResult = false;
        }
        return vResult;
    }
    // Hàm call api post new question
    function callApiPostNewQuestion(paramObj) {
        $.ajax({
            url: "http://localhost:8080/question/create",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(paramObj),
            async: false,
            success: function(res) {
                console.log(res);
                callApiGetAllQuestion();
                loadDataToTable();
                clearInpCreateQuestion();
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
    // Hàm hiển thị data vào modal update
    function showDataToModalUpdate(paramObj) {
        $('#inp-modal-question-id').val(paramObj.id);
        $('#inp-modal-question-code').val(paramObj.maCauHoi);
        $('#inp-modal-subject').val(paramObj.monHoc);
        $('#inp-modal-question-content').val(paramObj.noiDungCauHoi);
        $('#inp-modal-point').val(paramObj.diem);
    }
    // Hàm call api put question
    function callApiPutQuestion(paramObj) {
        console.log(paramObj.id);
        $.ajax({
            url: "http://localhost:8080/question/update/" + paramObj.id,
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(paramObj),
            async: false,
            success: function(res) {
                console.log(res);
                callApiGetAllQuestion();
                loadDataToTable();
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
    // Hàm call api delete question
    function callApiDeleteQuestion() {
        $.ajax({
            url: "http://localhost:8080/question/delete/" + gIdDelete,
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            async: false,
            success: function(res) {
                console.log(res);
                callApiGetAllQuestion();
                loadDataToTable();
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
    // Hàm xóa trắng input create
    function clearInpCreateQuestion() {
        $('#input-question-code').val("");
        $('#input-question-content').val("");
        $('#input-point').val("");
        $('#input-subject').val("");
    }
});