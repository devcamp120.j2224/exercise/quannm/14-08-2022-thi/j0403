$(document).ready(function() {
    // Vùng 1: Khai báo biến toàn cục
    var gArrayAnswer;
    var gIdDelete;
    var gArrayQuestion;
    var gNameCol = ["id", "maDapAn", "noiDungDapAn", "ketQuaDapAn", "giaiThich", "action"];
    const gID_COL = 0;
    const gMA_DAP_AN_COL = 1;
    const gNOI_DUNG_DAP_AN_COL = 2;
    const gKET_QUA_DAP_AN_COL = 3;
    const gGIAI_THICH_COL = 4;
    const gACTION_COL = 5;
    var gTableAnswer = $('#answer-table').DataTable({
        data: gArrayAnswer,
        columns: [
            {data: gNameCol[gID_COL]},
            {data: gNameCol[gMA_DAP_AN_COL]},
            {data: gNameCol[gNOI_DUNG_DAP_AN_COL]},
            {data: gNameCol[gKET_QUA_DAP_AN_COL]},
            {data: gNameCol[gGIAI_THICH_COL]},
            {data: gNameCol[gACTION_COL]}
        ],
        columnDefs: [
            {
                targets: gACTION_COL,
                defaultContent: "<button id='btn-update' class='btn btn-primary'>Update</button> &nbsp; <button id='btn-delete' class='btn btn-danger'>Delete</button>"
            }
        ]
    });

    // Vùng 2: Vùng gán
    onPageLoading();
    $('#btn-create-answer').on('click', onBtnCreateAnswerClick);
    $('#answer-table').on('click', '#btn-update', function() {
        onBtnUpdateAnswer(this);
    });
    $('#btn-update-answer-modal').on('click', onBtnUpdateAnswerInModalClick);
    $('#answer-table').on('click', '#btn-delete', function() {
        onBtnDeleteAnswer(this);
    });
    $('#btn-delete-answer-modal').on('click', onBtnDeleteAnswerInModalClick);

    // Vùng 3: Khai báo hàm xử lý sự kiện
    // Hàm xử lý khi tải trang
    function onPageLoading() {
        callApiGetAllAnswer();
        callApiGetAllQuestion();
        loadDataToTable();
    }
    // Hàm xử lý khi click btn create
    function onBtnCreateAnswerClick() {
        var vAnsObj = {
            maDapAn: "",
            noiDungDapAn: "",
            ketQuaDapAn: true,
            giaiThich: "",
            cauHoi: {
                id: ""
            }
        };
        vAnsObj.maDapAn = $('#input-answer-code').val();
        vAnsObj.noiDungDapAn = $('#input-answer-content').val();
        vAnsObj.ketQuaDapAn = $('#input-result').val();
        vAnsObj.giaiThich = $('#input-explain').val();
        vAnsObj.cauHoi.id = $('#input-question').val();
        console.log(vAnsObj);
        var vIsCheck = validateData(vAnsObj);
        if (vIsCheck == true) {
            callApiPostNewAnswer(vAnsObj);
        }
    }
    // Hàm xử lý khi click btn update
    function onBtnUpdateAnswer(paramEle) {
        var vTableRow = $(paramEle).parents("tr");
        var vAnswerRowData = gTableAnswer.row(vTableRow).data();
        console.log(vAnswerRowData);
        $('#modal-update-answer').modal("show");
        showDataToModalUpdate(vAnswerRowData);
    }
    // Hàm xử lý khi click update in modal
    function onBtnUpdateAnswerInModalClick() {
        var vAnsObj = {
            id: 0,
            maDapAn: "",
            noiDungDapAn: "",
            ketQuaDapAn: true,
            giaiThich: "",
            cauHoi: {
                id: ""
            }
        };
        vAnsObj.id = $('#inp-modal-answer-id').val();
        vAnsObj.maDapAn = $('#inp-modal-answer-code').val();
        vAnsObj.noiDungDapAn = $('#inp-modal-answer-content').val();
        vAnsObj.ketQuaDapAn = $('#inp-modal-result').val();
        vAnsObj.giaiThich = $('#inp-modal-explain').val();
        vAnsObj.cauHoi.id = $('#inp-modal-question').val();
        console.log(vAnsObj);
        var vIsCheck = validateData(vAnsObj);
        if (vIsCheck == true) {
            callApiPutAnswer(vAnsObj);
            $('#modal-update-answer').modal("hide");
        }
    }
    // Hàm xử lý khi click btn delete
    function onBtnDeleteAnswer(paramEle) {
        var vTableRow = $(paramEle).parents("tr");
        var vAnswerRowData = gTableAnswer.row(vTableRow).data();
        gIdDelete = vAnswerRowData.id;
        console.log(gIdDelete);
        $('#modal-delete-answer').modal("show");
    }
    // Hàm xử lý khi confirm delete
    function onBtnDeleteAnswerInModalClick() {
        callApiDeleteAnswer();
        $('#modal-delete-answer').modal("hide");
    }

    // Vùng 4: Khai báo hàm dùng chung
    // Hàm call api get all answer
    function callApiGetAllAnswer() {
        $.ajax({
            url: "http://localhost:8080/answer/all",
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res) {
                gArrayAnswer = res;
                console.log(gArrayAnswer);
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
    // Đổ dữ liệu vào datatable
    function loadDataToTable() {
        gTableAnswer.clear();
        gTableAnswer.rows.add(gArrayAnswer);
        gTableAnswer.draw();
    }
    // Validate data
    function validateData(paramData) {
        var vResult = true;
        if (paramData.maDapAn == "") {
            alert("Answer code chưa nhập!");
            vResult = false;
        }
        if (paramData.noiDungDapAn == "") {
            alert("Answer content chưa nhập!");
            vResult = false;
        }
        if (paramData.ketQuaDapAn == "") {
            alert("Result chưa nhập!")
            vResult = false;
        }
        if (paramData.ketQuaDapAn != "true" && paramData.ketQuaDapAn != "false") {
            alert("Result phải là true or false!")
            vResult = false;
        }
        if (paramData.giaiThich == "") {
            alert("Explain chưa nhập!")
            vResult = false;
        }
        if (paramData.cauHoi.id == "") {
            alert("Question chưa nhập!")
            vResult = false;
        }
        return vResult;
    }
    // call api get all question
    function callApiGetAllQuestion() {
        $.ajax({
            url: "http://localhost:8080/question/all",
            type: "GET",
            dataType: "json",
            async: false,
            success: function(res) {
                gArrayQuestion = res;
                console.log(gArrayQuestion);
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
    // Hàm call api post new question
    function callApiPostNewAnswer(paramObj) {
        $.ajax({
            url: "http://localhost:8080/answer/create",
            type: "POST",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(paramObj),
            async: false,
            success: function(res) {
                console.log(res);
                callApiGetAllAnswer();
                loadDataToTable();
                clearInpCreateAnswer();
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
    // Hàm xóa trắng input create
    function clearInpCreateAnswer() {
        $('#input-answer-code').val("");
        $('#input-answer-content').val("");
        $('#input-result').val("");
        $('#input-explain').val("");
        $('#input-question').val("");
    }
    // Hàm hiển thị data vào modal update
    function showDataToModalUpdate(paramObj) {
        $('#inp-modal-answer-id').val(paramObj.id);
        $('#inp-modal-answer-code').val(paramObj.maDapAn);
        $('#inp-modal-question').val(paramObj.cauHoi.id);
        $('#inp-modal-answer-content').val(paramObj.noiDungDapAn);
        $('#inp-modal-result').val(paramObj.ketQuaDapAn);
        $('#inp-modal-explain').val(paramObj.giaiThich);
    }
    // Hàm call api put question
    function callApiPutAnswer(paramObj) {
        console.log(paramObj.id);
        $.ajax({
            url: "http://localhost:8080/answer/update/" + paramObj.id,
            type: "PUT",
            dataType: "json",
            contentType: "application/json",
            data: JSON.stringify(paramObj),
            async: false,
            success: function(res) {
                console.log(res);
                callApiGetAllAnswer();
                loadDataToTable();
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
    // Hàm call api delete question
    function callApiDeleteAnswer() {
        $.ajax({
            url: "http://localhost:8080/answer/delete/" + gIdDelete,
            type: "DELETE",
            dataType: "json",
            contentType: "application/json",
            async: false,
            success: function(res) {
                console.log(res);
                callApiGetAllAnswer();
                loadDataToTable();
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }
});